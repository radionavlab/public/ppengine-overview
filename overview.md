# PpEngine Overview

PpEngine is a software-defined precise positioning engine based on loose or
tight coupling of carrier-phase-differential GNSS (CDGNSS) with inertial
measurements.  Built for extensibility, it can also ingest 
pseudorange measurements from terrestrial radio navigation systems and camera images.  PpEngine is
meant to run in conjunction with, and is dependent on, the [GRID Software
Suite](https://gitlab.com/radionavlab/core/gss) (GSS).

## General Software Characteristics

### Dependencies

PpEngine inherits the dependencies shown
    [here](https://rnl.ae.utexas.edu/wiki-rnl/doku.php?id=sw:pprx_install_dependencies)
    from GSS.  Other third-party code on which GSS depends is listed
    [here](https://gitlab.com/radionavlab/core/gss/-/tree/master/thirdparty?ref_type=heads).
    PpEngine has the additional dependencies listed
    [here](https://rnl.ae.utexas.edu/wiki-rnl/doku.php?id=sw:ppengine#ppengine_--_precise_positioning_engine).

### Test coverage

PpEngine's test suite consists of a set of integration tests and automated
checking tools, as described
[here](https://gitlab.com/radionavlab/core/rscore/-/blob/master/test/README.md).

### Build environment and tools

GSS's and PpEngine's build environment is based on [CMake](https://cmake.org/overview/).

### Number of lines of code

#### `gss/gbxframework`
```
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
C++                             53           1850           1681          14746
C/C++ Header                    73           1588           2523           8897
Protocol Buffers                49            170             93           1101
CMake                            2             20              2            194
Pascal                           1              0             37             61
-------------------------------------------------------------------------------
SUM:                           178           3628           4336          24999
-------------------------------------------------------------------------------
```


#### `gss/shared`
```
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
C++                            166           4443           7139          35411
C/C++ Header                   157           2287           3693          12687
CMake                            6             30              0            282
Protocol Buffers                 8             37            342            255
Markdown                         1              0              0              2
-------------------------------------------------------------------------------
SUM:                           338           6797          11174          48637
-------------------------------------------------------------------------------
```

#### `gss/pprx`
```
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
C++                             14            509            583           5246
Markdown                         2            124              0           1045
C/C++ Header                    13            159            214           1035
CMake                            2             21              0             86
Bourne Shell                     1              4             15             22
Protocol Buffers                 1              3              0             16
-------------------------------------------------------------------------------
SUM:                            33            820            812           7450
-------------------------------------------------------------------------------
```

#### `rscore/integerSolver`
```
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
C++                             12            385            561           2587
C/C++ Header                     5             54            188            667
MATLAB                          14             96            258            527
CMake                            1              1              0             14
-------------------------------------------------------------------------------
SUM:                            32            536           1007           3795
-------------------------------------------------------------------------------
```

#### `rscore/ppengine`
```
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
C++                             37           1329           1923          11021
C/C++ Header                    36            424           1527           2686
CMake                            1             11              0            102
Bourne Shell                     1              4             14             25
XML                              3              0              0             20
JSON                             1              0              0             12
-------------------------------------------------------------------------------
SUM:                            79           1768           3464          13866
-------------------------------------------------------------------------------
```

### Number of lines of test code

#### `rscore/test`
```
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                          29            825            788           3104
Markdown                         1             38              0            124
Bourne Shell                     1              5              1              5
-------------------------------------------------------------------------------
SUM:                            31            868            789           3233
-------------------------------------------------------------------------------
```

#### `rscore/integerSolver/testing`
```
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
C++                              3            111            104            838
-------------------------------------------------------------------------------
SUM:                             3            111            104            838
-------------------------------------------------------------------------------
```

### C++ version dependencies
```
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED YES)
```

## Estimator abstraction

PpEngine includes several different estimators. For its loosely-coupled
operational mode, some estimators act in a cascade, with RTK and
constrained-baseline estimators feeding a downstream estimator that draws in IMU
data, as shown in Fig. 3 [here](https://radionavlab.ae.utexas.edu/images/stories/files/papers/owvr_iongnss2020.pdf).
For its tightly-coupled operational mode, PpEngine starts off with cascaded estimation but switches to centralized estimation as soon as the central estimator's state has been initialized to sufficient accuracy.

All estimators within PpEngine, generically called PpEstimators, are implemented
as sigma-point filters (unscented Kalman filters).

###  What is the interface to the estimators in PpEngine?

[SigmaPointFilter](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/include/sigmapointfilter.h?ref_type=heads)
is an abstract class from which all filter implementations in PpEngine inherit.
[SpfConfig](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/include/spfconfig.h?ref_type=heads)
is its configuration object. The public functions of `SigmaPointFilter`
establish an interface for all derived filters.  The configuration options in
`SpfConfig` are accessible to all derived filters.

###  Is it easy to replace with another estimator?

It is easy to create new estimators that inherit from `SigmaPointFilter` and
from its derived estimators.  But it would be rather difficult to replace
`SigmaPointFiler` wholesale.

## Model abstraction

###  Is it easy to add/remove states (Fusion - add/remove IMU scale factor)?

Yes, have a look at how
[PoseAndTwist18](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/include/poseandtwistest18.h?ref_type=heads)
derives from
[PoseAndTwist15](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/include/poseandtwistest15.h?ref_type=heads),
adding three scalar states. 

### Is it easy to not use IMU as input, i.e., use other model as input?

[PositionRtk](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/include/positionrtk.h?ref_type=heads)
is an estimator that implements a simple single-baseline RTK solution with no
IMU inputs.  It supports the first four dynamics model types shown
[here](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/include/spfconfig.h?ref_type=heads#L34).

[Attitude2D](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/include/attitude2d.h?ref_type=heads)
is an estimator that implements a simple single-constrained-baseline
RTK solution with no IMU inputs.  It supports the `STATIC` model type shown
[here](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/include/spfconfig.h?ref_type=heads#L34).

The various pose estimators (e.g.,
[PoseAndTwist15](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/include/poseandtwistest15.h?ref_type=heads)
and others) support both the `BODY_NEARLY_CONSTANT_VELOCITY` (no IMU data) and
the `INERTIAL_MEASUREMENT_UNIT` dynamics models.


### For GNSS, how are dynamic states handled? (Add/remove SVs) And additional states like Iono?

No dynamically-sized states are currently carried forward in any of the filters.
At each epoch, a measurement update is performed in which an arbitrary number of
simultaneously-measured differenced carrier phase and pseudorange measurements are ingested.
After linearization about the current state estimate, a mixed integer/real
linear least squares cost function is formulated with the number of integer
states required to account for all integer ambiguities relevant at that epoch.
The cost function can be decomposed as shown in Eq. (4) of [Yoder et
al. (2023)](https://radionavlab.ae.utexas.edu/wp-content/uploads/2022/02/tight-coupling-journal.pdf).
This allows the integer part to be isolated from the real-valued part of the
state, and the cost function to be minimized first by solving for the optimal
integers (via integer least squares or ILS), then by conditioning on these
integers and solving for the real-valued part.

As can be seen by the state definition in Section II.B of [Yoder et
al. (2023)](https://radionavlab.ae.utexas.edu/wp-content/uploads/2022/02/tight-coupling-journal.pdf),
no integer terms are retained in the state.  Instead, all integer terms are
discarded from one epoch to the next.  This is effectively a posture of maximum
pessimism regarding cycle slips: each carrier tracking loop in the GNSS receiver
is assumed to slip cycles between each pair of measurement epochs. Ambiguity
state growth is curtailed by discarding all ambiguity states at every GNSS
measurement epoch, either by conditioning the state vector on the candidate
fixed solution (if the candidate fix is validated), or by accepting the float
solution and marginalizing over the ambiguities. More details may be found in
Section IV.D of [Yoder et
al. (2023)](https://radionavlab.ae.utexas.edu/wp-content/uploads/2022/02/tight-coupling-journal.pdf).

Because PpEngine's design focus has been on urban precise positioning, it is
built to operate in the so-called "short baseline regime" (defined
[here](https://iopscience.iop.org/article/10.1088/0957-0233/26/4/045801/pdf))
and thereby avoid the dilution of measurement model strength that occurs when
additional ionospheric states are added to the model.  Further justification and
support for this approach is found in [Murrian et
al. (2016)](https://radionavlab.ae.utexas.edu/images/stories/files/papers/murrianGpsWorldSeptember2016.pdf).

## Software Components and inter-component communication

### Is code clear in terms of software components and block diagram?

### Is interface between SWC well defined? Details of GBX format are needed.

PpEngine's major sofware components are each implemented as GBX Endpoints.  Each
Endpoint ingests and produces data in GBX streams, or streams of GBX-formatted
data.  Thus, GBX is the basic protocol by which major software components
communicate.  Examples of GBX Endpoint sinks and sources are seen
[here](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/src/ppengine.cpp?ref_type=heads#L112).

The GBX protocol format is described
[here](https://gitlab.com/radionavlab/core/gss/-/blob/master/doc/gbx/protocol-description-gbx.md?ref_type=heads).
The base GBX Endpoint object, from which all specialized Endpoints are derived,
is documented
[here](https://gitlab.com/radionavlab/core/gss/-/blob/master/gbxframework/include/gbxstream/endpoint.h?ref_type=heads#L28),
with additional documentation
[here](https://gitlab.com/radionavlab/core/gss/-/blame/master/gbxframework/include/gbxstream/common.h#L33). GBX
routing protocols are documented
[here](https://gitlab.com/radionavlab/core/gss/-/blob/master/gbxframework/include/gbxstream/router.h?ref_type=heads).

## State machine control

## How is the code controlled in different phases?
## State machine and state transition clear?

As seen
[here](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/include/ppengine.h?ref_type=heads#L44),
PpEstimator's process flow moves sequentially through the functions
`additionalSetup`, `spinUp`, `process`, and `shutDown`.  Within these functions,
process control is dictated by the sequential code and by the GBX Framework.  A
source of GBX data flows until exhaustion or until a specified data time
interval has elapsed, unless the user kills the process.  Thus, for example, the
process control within the `process` function remains at the function
[runUntilSourceDetach](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/src/ppengine.cpp?ref_type=heads#L631)
until the data are exhausted or a user-prompted interrupt occurs.

Process execution is multi-threaded, with separate threads for the various GBX
streams and data processing tasks running simultaneously.  Beyond what is
imposed by the PpEngine sequential functions (e.g., `spinUp` and `process`), by
GBX flow control, and by each GBX Endpoint as it reacts to incoming data, there
is no further state-machine-type state.

Within the GBX Endpoints, flow control is mediated by `bool` and `enum` type
variables; e.g.,
[skippingActive](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/include/endpointppestimator.h?ref_type=heads#L94).

## Time handling

### How are timestamps defined and used in the code?

All GBX reports either carry a timestamp or are ordered in the GBX protocol to
associate them with specific time-stamping reports.  Time stamping flows from
these reports through all GBX Endpoints that ingest them.  Time stamps are
typically expressed as
[BaseTime](https://gitlab.com/radionavlab/core/gss/-/blob/master/gbxframework/include/basetime.h?ref_type=heads)
objects, with an associated [time system](https://gitlab.com/radionavlab/core/gss/-/blob/master/gbxframework/messages/enumerations.proto?ref_type=heads#L28) as necessary.

## Testing

### Code testing is based on GBX format?

Yes.

### Can testing be done with the Septentrio GNSS input in the same data collection suite?

Yes, but a RINEX to GBX converter will have to be written.
 
## Ambiguity resolution

Our ambiguity process is fundamentally based on the optimal approach to mixed
real/integer linear data problems, called integer least squares (ILS).

### What advantage over LAMBDA?

We adopt a strategy for decorrelation adjustment that is slightly different from
Teunisson's LAMBDA algorithm but serves the same purpose, namely to speed up the
ILS solution by decorrelating the data to the extent possible, subject to
integer-only manipulations.  The algorithm we adopt for decorrelation adjustment
is based on one proposed by Psiaki in [Psiaki and Mohiuddin
(2007)](https://arc.aiaa.org/doi/pdf/10.2514/1.21982). It is comparable to
traditional LAMBDA for small problems with 3-10 integer unknowns, but it reduces
computational costs significantly when applied to larger problems with 11+
integer unknowns.

### How is the ambiguity verified?

Our
[IntegerApertureTest](https://gitlab.com/radionavlab/core/rscore/-/blob/master/integerSolver/include/integeraperturetest.h?ref_type=heads#L65)
class implements [three different types of
test](https://gitlab.com/radionavlab/core/rscore/-/blob/master/integerSolver/include/integeraperturetest.h?ref_type=heads#L67).
We typically use the `DIFFERENCE` test.  But much more goes into our ambiguity
verification, as detailed in Section IV of [Yoder et
al. (2023)](https://radionavlab.ae.utexas.edu/wp-content/uploads/2022/02/tight-coupling-journal.pdf).

### Is ambiguity resolution epoch-by-epoch?

Yes, but the state error covariance matrix involved in the ambiguity resolution
is typically small due to prior integer conditioning, which significantly
strengthens the model for ambiguity resolution.

### Does the UKF include ambiguity states?

No.  As explained above, all integer states are discarded after the measurement update.

### Is it similar to LAMBDA but uses QR-factorization in the linear solver?

Yes, our approach to the decorrelation adjustment is similar to LAMBDA, but more
computationally efficient for large models (many ambiguities).  Our
decorrelation adjustment is based on QR or LLL decomposition.

## GNSS stand-alone RTK

### L1 ambiguity vs WL ambiguity

We do not attempt to estimate widelane ambiguities because, since our ultimate
purpose is to resolve L1 ambiguities, there is no theoretical benefit to staged
ambiguity resolution (see papers by Teunisson).

### Any feedback from fixed ambiguity to float filter?

Optionally yes; see Section IV of [Yoder et
al. (2023)](https://radionavlab.ae.utexas.edu/wp-content/uploads/2022/02/tight-coupling-journal.pdf).

### How do you deal with long-baseline scenario?

Because PpEngine's design focus has been on urban precise positioning, it is
built to operate in the so-called "short baseline regime" (defined
[here](https://iopscience.iop.org/article/10.1088/0957-0233/26/4/045801/pdf))
and thereby avoid the dilution of measurement model strength that occurs when
additional ionospheric states are added to the model.  Further justification and
support for this approach is found in [Murrian et
al. (2016)](https://radionavlab.ae.utexas.edu/images/stories/files/papers/murrianGpsWorldSeptember2016.pdf).

We do implement a differential tropospheric model, which is essential even for
small differences in altitude between the reference and rover receivers.

Additional states for differential ionospheric errors could easily be added to
the state.  These would be appropriate in rural areas under a long-baseline
regime where multipath is less of a problem than ionospheric errors.

### Can you handle observations from multiple base stations?

We could ingest these by way of a Virtual Reference Station (VRS), but this will
require an RTCM to GBX converter.

## Tight coupling RTK

### How is tight coupling architected?

See Sections III and IV of [Yoder et
al. (2023)](https://radionavlab.ae.utexas.edu/wp-content/uploads/2022/02/tight-coupling-journal.pdf)
for the theoretical exegesis.  The implementation is found in
[PoseAndTwistEst15MultiModel](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/include/poseandtwistest15multimodel.h?ref_type=heads).

### Any false ambiguity fix monitor?

Yes.  See Section IV of [Yoder et
al. (2023)](https://radionavlab.ae.utexas.edu/wp-content/uploads/2022/02/tight-coupling-journal.pdf).

## Handover handling

### Ref SV change handling

We call the reference satellite the "pivot transmitter."  Its selection is dictated at each epoch by the
[choosePivot](https://gitlab.com/radionavlab/core/rscore/-/blob/master/ppengine/src/ppdatabundler.cpp?ref_type=heads#L311)
function.

### Do you reset all ambiguity states or carry over some information?

We discard all ambiguity states _but we carry forward the information they
provided via integer conditioning the real-values state elements on the fixed or
float integers_.

## Reference station 

### Base station change handling; the vehicle may move away from a base station and jump to another base station.

Not handled.

### SSR vs. OSR corrections: Is the system equipped to handle SSR corrections directly in any way or does it use the typical mock-OSR approach?

PpEngine is able to directly ingest precise orbits and clocks (e.g., from the
IGS), and local tropospheric parameters (e.g., from Weather Underground).  But
getting satellite-specific pseudorange and phase errors from SSR data is not yet
supported.  Rover-side SSR2OSR conversion would be straightforward to implement.
Native SSR support could also be implemented fairly straightforwardly by removing
satellite code and carrier biases from rover pseudorange and carrier phase
measurements and adopting a high-quality SSR-based ionospheric model.

### What correction provider/approach was used to generate the <20cm urban canyon run shown in the last presentation?

We ran an instance of GRID on our own base station, producing GBX-type
observables.  We also have RINEX data from a Septentrio AsteRX4 connected to the
same reference antenna over the same interval of time.
